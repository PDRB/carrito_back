
import spark.Request;
import spark.Response;
import spark.Route;
import static spark.Spark.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import spark.Filter;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import comunicaciones.StandarResponse;
import comunicaciones.StatusResponse;
import entidades.Compras;
import entidades.ImplementacionProductos;
import entidades.Producto;
import persistencia.HibernateUtil;



public class Aplicacion {

	public static void main(String[] args) {

		
		 ImplementacionProductos nuevo = new ImplementacionProductos();
		 cargarProductos(nuevo);
		ArrayList<String> productos = new ArrayList<String>();
		productos.add("tetera");
		productos.add("tesito");
		ArrayList<String> lista_de_productos = new ArrayList<String>();

	
		after((Filter) (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "GET");
            response.header("Access-Control-Allow-Methods", "POST");
        });
		
		
		get("/obtenertotal/", (request, response) -> {
			return "hola : " + request.headers("USER");
		});

		
		
		get("/enviarProductos", (request, response) -> {
			response.type("application/json");
			response.body("{"+
					"mensaje"+":" +"hola carola"+
					"}");
			
			  response.type("application/json");
			    return new Gson().toJson(
			      new StandarResponse(StatusResponse.SUCCESS,new Gson()
			        .toJsonTree(nuevo.getProductos())));
			
			
		});
		
		

		
		post("/cerrarCompra", (request, response) -> {
			// Create something
			try {
				Compras nueva = new Compras(obtenerFechaActual(), obtenerMontoTotal(), obtenerNumeroSocio((request.headers("USER"))));
				cerrarCompra(nueva);
			}
			catch (Exception e) {
				System.out.println(e.getMessage());
				return "ocurrio un error intente nuevamente";
			}
			return "Compra Cerrada con exito";
		});

		delete("/", (request, response) -> {
			JSONArray arr;
			try {
				String jsonString = request.body(); // assign your JSON String here
				JSONObject obj = new JSONObject(jsonString);
				arr = obj.getJSONArray("Productos");

				for (int i = 0; i < arr.length(); i++) {
					String post_id = arr.getJSONObject(i).getString("producto");
					productos.remove(productos.indexOf((post_id)));
				}
			} catch (Exception e) {
				return "ocurrio un error intente nuevamente";
			}
			return "se eliminaron correctamente " + arr.length() + " productos";
		});

		delete("/borrarCompra", (request, response) -> {
			productos.clear();
			return "se eliminaron correctamente los productos";
		});

	}



	private static void cerrarCompra(Compras nueva) {
		   Transaction transaction = null;
	        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
	            // start a transaction
	            transaction = session.beginTransaction();
	            // save the student objects
	            session.save(nueva);
	            // commit transaction
	            transaction.commit();

		            List < Compras > compras = session.createQuery("from Compras", Compras.class).list();
		          for (Compras venta : compras) {
		        	  System.out.println(venta.getNumero_de_socio());
				}
	        } catch (Exception e) {
	            if (transaction != null) {
	                transaction.commit();
	            }
	            e.printStackTrace();
	        }
		
	}

	private static String obtenerNumeroSocio(String user) {
	
		return "user";
	}



	private static double obtenerMontoTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	private static Date obtenerFechaActual() {
		Date date = new Date(System.currentTimeMillis());
		return date;
	}

	private static void cargarProductos(ImplementacionProductos nuevo) {
		
		   Transaction transaction = null;
		   double precio = 200 ;
		   Producto a = new Producto("tetera" , precio , "12350");
		   Producto b = new Producto("liston" , precio , "12350");
		   Producto c = new Producto("jarra" , precio , "12350");
	        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
	            // start a transaction
	            transaction = session.beginTransaction();
	            // save the student objects
	            session.save(a);
	            session.save(b);
	            session.save(c);
	            // commit transaction
	            transaction.commit();

		            List < Producto > almacen = session.createQuery("from Producto", Producto.class).list();
		          for (Producto producto : almacen) {
		        	System.out.println(nuevo);
		        	
		        	  nuevo.addProducto(producto);
				}	}
		
	}
	
	


	private static JSONObject obtenerProductos(ArrayList<String> productos) {
		
		String envio = "{ productos : ";
		for (int i = 0; i < productos.size(); i++) {
			envio = "[" + envio + productos.get(i) + "],"; 
		
		}
		
		JSONObject json = new JSONObject(envio);
		return json;
	}
}