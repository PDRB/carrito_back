package negocio;

import java.util.ArrayList;
import java.util.Date;

import entidades.Compras;
import entidades.Producto;
import entidades.Usuario;



public abstract class Carrito {

	
	private ArrayList<Producto> lista_a_comprar;
	
	private double importe;
	
	private Usuario comprador;
	
	private Date fecha_factura;
	
	private double monto_a_descontar;
	
	public Carrito(Usuario comprador){
		
		
		this.comprador = comprador;
		lista_a_comprar = new ArrayList<Producto>();
		importe = 0;
		monto_a_descontar = 0;
	    this.fecha_factura = new Date();  
	}
	
	private void calcularDescuentoX5Unidades(){
		
	}
	
	
	abstract void calcularDescuentoX10unidades();
		
	
	public void agregarAlCarro(Producto a_cargar){
		
		lista_a_comprar.add(a_cargar);
	}
	
	public double obtenerTotal(){
		return importe;
	}
	
	private void DestruirCarro(){
		this.comprador = null;
		this.lista_a_comprar = null;
		this.importe = 0;
		this.fecha_factura = null;
	
	}
	public Compras cerrarCompra(){
		
		
		Compras venta = new Compras(this.fecha_factura , this.importe , this.comprador.getIdentificador_cliente());
		DestruirCarro();
		return venta;
	
	}
	
	
}
