package entidades;

import java.util.Collection;

public interface ServicioDeProductos {

	
	
		 
	    public void addProducto (Producto Producto);
	    
	    public Collection<Producto> getProductos ();
	    public Producto getProducto (String id);
	    public Producto editProducto (Producto user) ;
	    public void deleteProducto (String id);
	    public boolean ProductoExist (String id);
	
}
