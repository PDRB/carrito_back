package entidades;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Producto")
public class Producto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "monto")
    private Double monto;

    @Column(name = "cod_art")
    private String cod_art;

  public Producto () {
	  
	  
  }

    public Producto(String descripcion, Double monto, String cod_art) {
        this.descripcion = descripcion;
        this.monto = monto;
        this.cod_art = cod_art;
    }

    public int getId() {
        return id;
    }

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public String getCod_art() {
		return cod_art;
	}

	public void setCod_art(String cod_art) {
		this.cod_art = cod_art;
	}

	@Override
	public String toString() {
		return "Producto [descripcion=" + descripcion + ", monto=" + monto + ", cod_art=" + cod_art + "]";
	}

	
}