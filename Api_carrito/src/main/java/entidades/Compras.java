package entidades;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Compras
 *
 */
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Compra")
public class Compras implements Serializable {

    public Compras() {

    } 
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;


	@Column(name = "fecha_factura")
    private Date fecha_factura;

    @Column(name = "monto")
    private double monto;

    @Column(name = "numero_de_socio")
    private String numero_de_socio;
    
    

public Compras(Date fecha , double monto , String numero_socio) {
	this.fecha_factura = fecha ;
	this.monto = monto;
	this.numero_de_socio = numero_socio;
	
}

   
    public Date getFecha_factura() {
	return fecha_factura;
}






public void setFecha_factura(Date fecha_factura) {
	this.fecha_factura = fecha_factura;
}



public double getMonto() {
	return monto;
}



public void setMonto(double monto) {
	this.monto = monto;
}



public String getNumero_de_socio() {
	return numero_de_socio;
}



public void setNumero_de_socio(String numero_de_socio) {
	this.numero_de_socio = numero_de_socio;
}




@Override
	public String toString() {
		return "Compras [id=" + id + ", fecha_factura=" + fecha_factura + ", monto=" + monto + ", numero_de_socio="
				+ numero_de_socio + "]";
	}

   
}
